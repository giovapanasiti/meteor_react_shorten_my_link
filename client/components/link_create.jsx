import React, {Component} from 'react';

class LinkCreate extends Component {
  // this is to init the state
  constructor(props){
    super(props);

    this.state= {error: ''}
  }

  handleSubmit(e) {
    e.preventDefault();
    
    console.log(this.refs.link.value);
    Meteor.call('links.insert', this.refs.link.value, /*call back*/ (error) => {
      if (error) {
        this.setState({error: 'Enter a Valid URL'});
      } else {
        this.setState({error: ''});
        this.refs.link.value = '';
      }
    });
    
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <div className="form-group">
          <label > Link</label>
          <input ref="link" className="form-control" />
        </div>
        <div className="text-danger">{this.state.error}</div>
        <button className="btn btn-primary">Shorten</button>
      </form>
    );
  }
}

export default LinkCreate;