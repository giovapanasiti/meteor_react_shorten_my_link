import { Meteor } from 'meteor/meteor';
import {Links} from '../imports/collections/links';
import ConnectRoute from 'connect-route';

// middleware (middleware is just a function that gets called each time a request comes in)
import { WebApp } from 'meteor/webapp';


Meteor.startup(() => {
  // code to run on server at startup

  Meteor.publish('links', function(){
    return Links.find({});
  });
});

// executed whenever a user visits with a route like 'localhost:3000/abcd'
function onRoute(req, res, next) {
  // take the token out of the url and try to find a matching link in the Links collections
  const link = Links.findOne({token: req.params.token});
  
  if (link) {
    // if we find a link object, redirect the user to the long url and update the click count
    Links.update(link, {$inc: {clicks: 1}});

    res.writeHead(307, {Location: link.url});
    res.end();
  } else {
    // if we don't find a link object send the user to our normal react app
    next();
    // this tells to go to the next middleware. in this case the people will be redirect to our app
  }
}

// localhost:3000   -> NO MATCH
// localhost:3000/books/ciao   -> NO MATCH
// localhost:3000/anything   -> !!!!MATCH!!!!

const middleware = ConnectRoute(function(router){
  router.get('/:token', onRoute);
});

WebApp.connectHandlers.use(middleware);