import {Mongo} from 'meteor/mongo';
import validUrl from 'valid-url';
import {check, Match} from 'meteor/check';

Meteor.methods({
  'links.insert': function(url) {
    console.log('save this url:', url);
    // validUrl.isUri(url);
    // if it is valid return the URL otherwise return undefined
    // we use "check"" to validate and if it fails it will throw an error
    check(url, Match.Where(url=>validUrl.isUri(url)));
    // maTCH.where to a custoom validation. if valid returns true the check skip and return url 
    // if it is not cuastom message and validation come

    // we are ready to save the URL here after the check
    const token = Math.random().toString(36).slice(-5);

    Links.insert({url: url, token: token, clicks: 0})
    // es6 version: Links.insert({url, token, clicks: 0})
    
  }
});

export const Links = new Mongo.Collection('links');